var express = require('express');
var path = require('path');
fs = require('fs');
var cookieParser = require('cookie-parser');
var app = express();
app.use(cookieParser());
const axios = require('axios');

//app is server with end point URL i.e Server URL which client user calls
// i.e http://localhost:3000/pdf_file
app.get('/pdf_file', function (req, res) {
    //PDF File Location
    var file = path.join(__dirname, 'iLocker-Handbook-V5.pdf');
    console.log(req.cookies['_ga']);

    // Getting Client Id from the request
    var tvc_ga_ck_fetch = req.cookies['_ga'];
    var tvc_cid = tvc_ga_ck_fetch.split('.')[2] + '.' + tvc_ga_ck_fetch.split('.')[3];
    //Creating Measurement Protocol Get request URL for GA with client ID from the request header
    // Also set the UA Property ID 
    var end_url = 'https://www.google-analytics.com/collect?v=1&t=event&tid=UA-XXXXXXXXX-X&cid=' + tvc_cid + '&dp=%2Fpdf_file&ec=PDF%20file%20View%2FDownload&ea=' + tvc_cid;


    //Getting and sending the pdf file to the user browser to view it
    var data = fs.readFileSync(file);
    res.contentType("application/pdf");
    res.send(data);

    //Code if Server sends the file to auto download instead of viewing directly on the browser
    //    res.download(file, function (err) {
    //        if (err) {
    //            console.log("Error");
    //            console.log(err);
    //        } else {
    //            console.log("Success");
    //        }
    //    });

//code to send MP Hit once the pdf file
    axios.get(end_url)
        .then(response => {
            // console.log(response);
            console.log('Response Code');
            console.log(response.status);
        })
        .catch(error => {
            console.log(error);
        });
});

//Server of nodejs 
var server = app.listen(3000, function () {
    console.log('Listening on', server.address().port);
});

